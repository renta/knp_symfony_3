<?php

namespace App\Controller\Admin;

use App\Form\GenusFormType;
use App\Entity\Genus;
use App\Service\MessageManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/admin")
 * @IsGranted("ROLE_MANAGE_GENUS")
 */
class GenusAdminController extends Controller
{
    /**
     * @Route("/genus", name="admin_genus_list")
     */
    public function indexAction()
    {
        // 1 way
        /*if (!$this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')) {
            throw $this->createAccessDeniedException('Get out!!!');
        }*/
        // 2 way
        //$this->denyAccessUnlessGranted('ROLE_ADMIN');
        // 3 way - method or class annotation

        $genuses = $this->getDoctrine()
            ->getRepository(Genus::class)
            ->findAll();

        return $this->render('admin/genus/list.html.twig', array(
            'genuses' => $genuses
        ));
    }

    /**
     * @Route("/genus/new", name="admin_genus_new")
     */
    public function newAction(Request $request)
    {
        $form = $this->createForm(GenusFormType::class);

        //only handles data on POST
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            //dump($form->getData()); die();
            $genus = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash(
                'success',
                sprintf('Genus created - you (%s) are amazing!', $this->getUser()->getEmail())
            );

            return $this->redirectToRoute('admin_genus_list');
        }

        return $this->render('admin/genus/new.html.twig', [
            'genusForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/genus/{id}/edit", name="admin_genus_edit")
     *
     * @param Request $request
     * @param Genus $genus
     * @param MessageManager $messageManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function editAction(Request $request, Genus $genus, MessageManager $messageManager)
    {
        $form = $this->createForm(GenusFormType::class, $genus);

        //only handles data on POST
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //dump($form->getData()); die();
            $genus = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($genus);
            $em->flush();

            $this->addFlash('success', 'Genus updated');

            //return $this->redirectToRoute('admin_genus_list');

            $this->addFlash(
                'success',
                $messageManager->getEncouragingMessage()
            );

        }  elseif ($form->isSubmitted()) {

            $this->addFlash(
                'error',
                $messageManager->getDiscouragingMessage()
            );
        }

        return $this->render('admin/genus/edit.html.twig', [
            'genusForm' => $form->createView()
        ]);
    }
}
