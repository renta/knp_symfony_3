<?php

namespace App\Controller;

use App\Entity\Genus;
use App\Entity\GenusNote;
use App\Entity\GenusScientist;
use App\Entity\SubFamily;
use App\Entity\User;
use App\Repository\GenusRepository;
use App\Service\MarkdownTransformer;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class GenusController extends Controller
{
    /**
     * @Route("/genus/new")
     * @IsGranted("RANDOM_ACCESS")
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $subFamily = $em->getRepository(SubFamily::class)
            ->findAny();

        $genus = new Genus();
        $genus->setName("Octopus" . rand(1, 1000));
        $genus->setSubFamily($subFamily);
        $genus->setSpeciesCount(rand(100, 99999));
        $genus->setFirstDiscoveredAt(new \DateTime('50 years'));

        $genusNote = new GenusNote();
        $genusNote->setUsername('AquaWeaver');
        $genusNote->setUserAvatarFilename('ryan.jpeg');
        $genusNote->setNote('8 legs and bla-bla-bla...');
        $genusNote->setCreatedAt(new \DateTime('-1 month'));
        $genusNote->setGenus($genus);

        $user = $em->getRepository(User::class)
            ->findOneBy(['email' => 'aquanaut1@example.org']);

        $genusScientist = new GenusScientist();
        $genusScientist->setGenus($genus);
        $genusScientist->setUser($user);
        $genusScientist->setYearsStudied(10);

        $em->persist($genusScientist);
        $em->persist($genus);
        $em->persist($genusNote);
        $em->flush();

        return new Response(
            sprintf(
                "<html><body>Genus created! <a href='%s'>%s</a></body></html>",
                $this->generateUrl('genus_show', ['slug' => $genus->getSlug()]),
                $genus->getName()
            )
        );
    }

    /**
     * @Route("genus")
     *
     * @param GenusRepository $genusRepository
     *
     * @return Response
     */
    public function listAction(GenusRepository $genusRepository)
    {
        $genuses = $genusRepository->findAllPublishedOrderedByRecentlyActive();

        return $this->render("genus/list.html.twig", [
            'genuses' => $genuses,
        ]);
    }

    /**
     * @Route("/genus/{slug}", name="genus_show")
     *
     * @param Genus $genus
     * @param MarkdownTransformer $markdownTransformer
     *
     * @return Response
     */
    public function showAction(Genus $genus, MarkdownTransformer $markdownTransformer, LoggerInterface $logger)
    {
        $em = $this->getDoctrine()->getManager();

        $funFact = $markdownTransformer->parse($genus->getFunFact());

        $logger->info('Showing genus: ' . $genus->getName());
        /*$cache = $this->get('doctrine_cache.providers.my_markdown_cache');
        $key = md5($funFact);

        if ($cache->contains($key)) {
            $funFact = $cache->fetch($key);
        } else {
            sleep(1);
            $funFact = $this->get('markdown.parser')->transform($funFact);
            $cache->save($key, $funFact);
        }*/

        /*$recentNotes = $genus->getNotes()->filter(function(GenusNote $note) {
            return $note->getCreatedAt() > new \DateTime('-3 months');
        });*/

        $recentNotes = $em->getRepository(GenusNote::class)
            ->findAllRecentNotesForGenus($genus);

        return $this->render("genus/show.html.twig", [
            "genus"            => $genus,
            "recentNoteCount" => count($recentNotes),
            "funFact"         => $funFact,
        ]);
    }

    /**
     *
     * @Route("/genus/{slug}/notes", name="genus_show_notes")
     * @Method("GET")
     * @param Genus $genus
     * @return Response json
     */
    public function getNotesAction(Genus $genus)
    {
        $notes = [];
        foreach ($genus->getNotes() as $note) {
            $notes[] = [
                'id'        => $note->getId(),
                'username'  => $note->getUsername(),
                'avatarUri' => '/images/' . $note->getUserAvatarFilename(),
                'note'      => $note->getNote(),
                'date'      => $note->getCreatedAt()->format('M d, Y'),
            ];
        }

        $data = [
            "notes" => $notes,
        ];

        return new JsonResponse($data);
    }

    /**
     * @Route("/genus/{genusId}/scientist/{userId}", name="genus_scientist_remove")
     * @Method("DELETE")
     */
    public function removeGenusScientistAction($genusId, $userId)
    {
        $em = $this->getDoctrine()->getManager();

        $genusScientist = $em->getRepository(GenusScientist::class)
            ->findOneBy([
                'user' => $userId,
                'genus' => $genusId
            ]);

        $em->remove($genusScientist);
        $em->flush();

        return new Response(null, 204);
    }
}
